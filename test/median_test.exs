defmodule MedianTest do
  use ExUnit.Case
  doctest Median

  test "NIF init" do
    assert Median.init() == :ok
  end

  test "empty list" do
    Median.init()
    assert Median.select([]) == nil
  end

  test "trivial lists of integers" do
    Median.init()
    assert Median.select([1]) == 1
    assert Median.select([1, 2]) == 2
  end

  test "trivial lists of floats" do
    Median.init()
    assert Median.select([1.0]) == 1.0
    assert Median.select([1.0, 2.0]) == 2.0
  end

  test "lists of integers of length 3" do
    Median.init()
    assert Median.select([1, 2, 3]) == 2
    assert Median.select([1, 3, 2]) == 2
    assert Median.select([2, 1, 3]) == 2
    assert Median.select([2, 3, 1]) == 2
    assert Median.select([3, 1, 2]) == 2
    assert Median.select([3, 2, 1]) == 2
  end

  test "lists of floats of length 3" do
    Median.init()
    assert Median.select([1.0, 2.0, 3.0]) == 2.0
    assert Median.select([1.0, 3.0, 2.0]) == 2.0
    assert Median.select([2.0, 1.0, 3.0]) == 2.0
    assert Median.select([2.0, 3.0, 1.0]) == 2.0
    assert Median.select([3.0, 1.0, 2.0]) == 2.0
    assert Median.select([3.0, 2.0, 1.0]) == 2.0
  end

  test "lists of integers of length 4" do
    Median.init()
    assert Median.select([1, 2, 3, 4]) == 3
    assert Median.select([3, 1, 4, 2]) == 3
    assert Median.select([4, 3, 2, 1]) == 3
  end

  test "lists of floats of length 4" do
    Median.init()
    assert Median.select([1.0, 2.0, 3.0, 4.0]) == 3.0
    assert Median.select([3.0, 1.0, 4.0, 2.0]) == 3.0
    assert Median.select([4.0, 3.0, 2.0, 1.0]) == 3.0
  end

  test "static lists of integers" do
    Median.init()
    l_9  = [4, 2, 5, 7, 9, 6, 1, 3, 8]
    l_10 = [1, 9, 7, 3, 2, 5, 10, 8, 6, 4]
    l_11 = [8, 2, 9, 4, 6, 5, 3, 11, 10, 1, 7]
    l_12 = [3, 2, 5, 7, 8, 1, 4, 12, 10, 11, 9, 6]
    l_21 = [6, 12, 15, 11, 2, 13, 10, 19, 20, 18, 1, 5, 9, 7, 8, 16, 3, 17, 21, 4, 14]
    assert Median.select(l_9) == 5
    assert Median.select(l_10) == 6
    assert Median.select(l_11) == 6
    assert Median.select(l_12) == 7
    assert Median.select(l_21) == 11
  end

  test "static lists of floats" do
    Median.init()
    l_9  = [4.0, 2.0, 5.0, 7.0, 9.0, 6.0, 1.0, 3.0, 8.0]
    l_10 = [1.0, 9.0, 7.0, 3.0, 2.0, 5.0, 10.0, 8.0, 6.0, 4.0]
    l_11 = [8.0, 2.0, 9.0, 4.0, 6.0, 5.0, 3.0, 11.0, 10.0, 1.0, 7.0]
    l_12 = [3.0, 2.0, 5.0, 7.0, 8.0, 1.0, 4.0, 12.0, 10.0, 11.0, 9.0, 6.0]
    l_21 = [6.0, 12.0, 15.0, 11.0, 2.0, 13.0, 10.0, 19.0, 20.0, 18.0, 1.0, 5.0, 9.0, 7.0, 8.0, 16.0, 3.0, 17.0, 21.0, 4.0, 14.0]
    assert Median.select(l_9) == 5.0
    assert Median.select(l_10) == 6.0
    assert Median.select(l_11) == 6.0
    assert Median.select(l_12) == 7.0
    assert Median.select(l_21) == 11.0
  end

  test "invalid types" do
    Median.init()
    [nil, 123, [], ["hello"]]
    |> Enum.each(fn x ->
      try do
        Median.select(x)
      rescue
        e -> assert e == %ArgumentError{message: "argument error"}
      end
    end)
  end

  test "invalid mixed lists" do
    Median.init()
    [[1, 2, 3.0], [1.0, 2.0, 3]]
    |> Enum.each(fn x ->
      try do
        Median.select(x)
      rescue
        e -> assert e == %ArgumentError{message: "argument error"}
      end
    end)
  end
end
