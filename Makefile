CFLAGS += -Wall -std=c99 -fPIC -O3
ERL_PATH = $(shell erl -eval "io:format(\"~s~n\", [code:root_dir()])" -s init stop -noshell)
ERL_INCLUDE_PATH = $(ERL_PATH)/usr/include/

all: clean priv/median.so

test: clean median_test
	@./median_test

.PHONY: clean
clean:
	@rm -rf priv/
	@rm -f median_test

priv/median.so: src/median.c src/median_int.h src/median_float.h
	@mkdir -p $(@D)
	$(CC) $(CFLAGS) -shared -I$(ERL_INCLUDE_PATH) $^ -o $@

median_test: test/median_test.c src/median_int.h src/median_float.h
	$(CC) $(CFLAGS) -g -I$(ERL_INCLUDE_PATH) $^ -o $@

