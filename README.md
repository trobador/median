# Median

Fast median selection implemented in C for Elixir through a dirty NIF.

## Installation

The package can be installed by adding `median` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:median, "~> 0.1.1"}
  ]
end
```

## Basic Usage

```elixir
iex(1)> Median.select [5, 2, 4, 1, 3]
3
```

