defmodule Median do
  @moduledoc """
  Documentation for `Median`.
  """

  require Logger

  @on_load :init

  # XXX only for testing
  def random_list(n) do
    (for n <- 1..n, do: {:rand.uniform(), n})
    |> Enum.sort
    |> Enum.map(fn {_, n} -> n end)
  end

  def init() do
    file = Application.app_dir(:median, ~w(priv median))
    case :erlang.load_nif(file, 0) do
      :ok -> :ok
      {:error, {:reload, _text}} -> :ok
      {:error, {_reason, text}} -> Logger.error(text)
    end
  end

  def select(_), do: raise "NIF library not loaded"
end
