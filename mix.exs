defmodule Median.MixProject do
  use Mix.Project

  def project do
    [
      app: :median,
      version: "0.1.1",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      compilers: [:elixir_make] ++ Mix.compilers,
      description: description(),
      package: package(),
      source_url: "https://gitlab.com/trobador/median",
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:elixir_make, "~> 0.6.0", runtime: false}
    ]
  end

  defp description() do
    "Fast median selection implemented in C for Elixir through a dirty NIF."
  end

  defp package() do
    [
      files: ["lib", "src", "mix.exs", "Makefile", "LICENSE", "README.md"],
      licenses: ["Apache-2.0"],
      links: %{"GitLab" => "https://gitlab.com/trobador/median"}
    ]
  end
end
