#include <stdio.h>
#include "../src/median_int.h"

void test_int(int arr[], unsigned int len, unsigned int pad) {
	if (pad == len) {
		/*
		printf("[");
		for (unsigned int i=0; i<(len-1); i++)
			printf("%i, ", arr[i]);
		printf("%i]\n", arr[len-1]);
		*/

		int m = median_int(arr, len);
		if (m != (len/2+1))
			printf("ERROR: Expected %i, got %i\n", (len/2+1), m);
	} else {
		for (unsigned int i = pad; i < len; i++) {
			swap_int(&arr[pad], &arr[i]);
			test_int(arr, len, pad+1);
			swap_int(&arr[pad], &arr[i]);
		}
	}
}

int main() {
	int arr[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	unsigned int len = sizeof(arr) / sizeof(int);

	printf("Testing permutations:\n");
	for (unsigned int i = 1; i<=len; i++) {
		printf("%u of %u...\n", i, len);
		test_int(arr, i, 0);
	}

	return 0;
}

